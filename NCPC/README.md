## NCPC compendium

- **Perturb_reTFs.rds**: a compendium of response transcriptional signatures (DETFs) to each signalling perturbagen.	

- **Perturb_rtargets.rds**: a compendium of the protein targets of the signalling perturbagens in Perturb_reTFs.rds.
- **pert_target_all_human.rds**: a compendium of the protein targets of signalling perturbagens reported in public databases, besides the perturbagens present in Perturb-reTFs.rds.

- **pert_target_all_mouse.rds**: same as pert_target_all_human.rds, but human gene symbols are converted into mouse homologous symbols.

- **perturbagens_categories_annotation.rds**: the perturbagens are categorized into different groups based on their usability in different research areas, including cellular reprogramming, cell cycle, metabolism, immunology, as well as FDA approved drug. In addition, the functional mechanism is annotated for each perturbagen. 
# SiPer

A computational-guided framework to identify chemical compounds targeting specific sets of transcription factors for chemical-induced cellular conversion.


This repository presents a computational framework that predicts signalling proteins and chemical compounds(perturbagens) specifically targeting a desired set of TFs provided by the user to induce cellular conversion.

To install it:
```bash
git clone https://git-r3lab.uni.lu/menglin.zheng/SiPer.git path/to/workdir
cd path/to/workdir
```
## Requirements
SiPer was implemented in R and it has been teseted on Unix environment with R version 3.6.0.
### R packages
SiPer requires the packages: "tibble", "plyr", "dplyr", "igraph", "foreach", "doParallel", "data.table", "tidyr", "Seurat", "devtools", "biomaRt", "RCy3" and "WebGestaltR","writexl","readxl". To test if you have the required packages and automatcially install the missing packages, run `R_package_install.R`.
### Cytoscape
SiPer supports the visualization of the paths between perturbagens and query TFs, which is achieved by using Cytoscape that accesses per R package Rcy3. Rcy3 requires Cytoscape 3.6.1 or greater, which can be downloaded from http://www.cytoscape.org/download.php.
## Usage
### Input file formats
Two input files are required for SiPer:
1. scRNA data of initial cellular state in rds/txt format. This file contains the scRNA-seq data of initial cellular state. Each row represents gene and each column represents cell, which can be data frame or Seurat object (See example in `testdata/H9_p_E-MTAB-6819.rds`). 
2. query TFs in rds/txt format. This file contains a TF vector with gene names and the value of TFs are 1/-1, which means activation/inhibition (See example in `testdata/hESCp2n_TFs.txt` ).


### Parameters
There are three parameters in the pipeline:
- **species**: only for human/mouse/rat.
- **perturbagen_num**: number of selected perturbagens in each functional group. Default value is 10.
- **function_category**: perturbagens category, available for "Reprogramming","Cell.cycle","Metabolism","Immunology","FDA.Approved.Drug". Users of SiPer can use this information to futher narrow down candidate perturbagens, depending on the specific application fields. Users can select one or mulitple categories seperated by comma (e.g. Reprogramming,Cell.cycle,Metabolism).  Default value is NULL, which uses all the collected perturbagens except "FDA.Approved.Drug", as these are usually used for chemotherapies and are not relevant to SiPer's objective.
### Run test dataset
We need query TFs and scRNA-seq data of initial cellular state as input files. The phenotypic conversion from primed hESCs to naive hESCs are contained in this repository. To run SiPer:
```bash
Rscript SiPer_pipeline.R ../testdata/H9_p_E-MTAB-6819.rds ../testdata/hESCp2n_TFs.txt human 10 NULL
```
The web application is available at https://siper.uni.lu.

The output files after running are presented in `output/testdata_result`, including 3 files:
- **contextualized_PKN.rds**: This file contains the contextualized PKN based on initial cellular state, mainly used for network visualization. 
- **predicted_signalling_protein.xlsx**: This file contains the list of predicted signalling proteins (column "Protein") and their corresponding rank (column "Rank"), sign (column "Sign") and JSD score (column "JSD_val"). The column "Sign" including values "1", "-1" and "2", which mean the corresponding protein should be activated, inhibited or unknown, respectively.
- **Annotated_perturbagens.xlsx**: This file contains the list of predicted perturbagens and their corresponding information. Column "Target" presents the  targets predicted by SiPer. Columns “Chemical_Effect” and “Predicted_Effect” shows the interaction effect between perturbagens and signalling proteins that are reported in public databases and our prediction, respectively. Value 1, -1 and 2 means activation, inhibition and unknown, respectively. Column "Group" seperates the perturbagens into different groups based on their protein targets ("Other" merges the groups with perturbagen less than 3). Column "Category" presents the usability of the perturbagen in different research areas. Column "Mechanism" reports the functional annotation. Column "Jaccard" presents the Jaccard value for each perturbagen.


### Network visualization
SiPer supports the visualization of signalling cascades between predicted perturbagens, the predicted targets of perturbagens, intermediate signalling proteins and query TFs. To visualize the paths between predicted perturbagens of users’ interest and query TFs, for example, between perturbagens "pd 0325901","chir99021","forskolin","wh-4-023" to query TFs in the induction of primed hESCs from primed hESCs, using the function "visualization_network" as follows:
```bash
visualization_network('../output/testdata_result/contextualized_PKN.rds','../output/testdata_result/Annotated_perturbagens.xlsx','../testdata/hESCp2n_TFs.rds',c("pd 0325901","chir99021","forskolin","wh-4-023"),'human','My_network')
```
There are six parameters in the network visualization:
- the output file of contextualized PKN
- the output file of predicted perturbagens with annotation
- the file of query TFs
- a vector of predicted perturbagens of users’ interest
- species
- the name of network

**Notably**, Cytoscape needs to be open while running the visualization function.


![uni.lu.svg](/uploads/f35dbc86f9d81f46c98f0971eee093ca/uni.lu.svg)


